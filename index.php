<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel=icon href=favicon.png />
<title>Lucas Bonavina - Desenvolvedor Freelancer</title>
<meta name=author content="Lucas Bonavina - http://lucasbonavina.com">
<meta name=keywords content="web, sites, develop, desenvolvedor, freelancer, java, html, php, wordpress, android, sao paulo, desenvolvedor freelancer, backend, frontend">
<meta name=description content="Desenvolvedor Back-end e Front-end - Linguagens como Java, php, html5, css3, desenvolvimento android entre outros.">
<meta name=viewport content="width=device-width">
<meta name="expires" content="Sun, 25 Sep 2016 GMT">
<meta property="og:title" content="Lucas Bonavina - Desenvolvedor Freelancer" />
<meta property="og:description" content="Desenvolvedor Back-end e Front-end - Java, php, html5, css3, e outros." />
<meta property="og:image" content="images/og-image.png" />
<style>
#logo,#menu{height:80px}#texto-welcome,#titulo-welcome,.menu-item{color:#fff;text-align:center}#menu,#porque{box-shadow:0 0 10px 0 #000}body{margin:0;padding:0;font-family:Lato,sans-serif;background-color:#222;color:#fff}a{text-decoration:none}.container{width:1000px;margin-left:auto;margin-right:auto;position:relative}.conteudo{width:100%}#menu{background-color:#00a2ff;border-bottom:solid 3px #0095ea}#logo{float:left}#menu-itens{height:60px;padding:12px 0 15px;float:right}.menu-item{background-color:transparent;padding:15px 10px;margin-right:10px;margin-left:10px;font-weight:900;font-size:20px;text-transform:uppercase;float:left;text-shadow:0 2px #0095EA}.menu-item:hover{transition:.2s;-webkit-transition:.2s;transform:scale(1.2)}#titulo-welcome{margin:25px auto auto;display:block;padding:10px;font-weight:900;font-size:50px;max-width:80%;border-bottom:solid 2px #fff}video{position:fixed;top:50%;left:50%;min-width:100%;min-height:100%;width:auto;height:auto;z-index:-100;transform:translateX(-50%) translateY(-50%);background-size:cover;transition:1s opacity;opacity:.4}#texto-welcome{margin:50px 0;font-size:25px}.blue{color:#00A2FF}.button-contato,.social-button{color:#fff;font-size:23px;font-weight:700;border-radius:5px;background-color:#00A2FF;border-bottom:solid 4px #0095EA;text-shadow:0 2px #007FC8}#social-buttons{margin-right:auto;margin-left:auto;text-align:center;display:table;margin-bottom:50px}.item-carousel,.item-carousel-cliente{margin-right:25px;margin-left:25px}.social-button{padding:20px;float:left;margin:25px}.button-contato{width:100%;padding-top:25px;padding-bottom:25px}#contato,#porque{padding-top:25px;padding-bottom:50px}#portfolio,#servicos{padding-top:50px;padding-bottom:75px;color:#666;text-align:center}#footer{background-color:#f3f3f3;text-align:center;padding:25px 0;color:#555}.button-contato:hover,.social-button:hover{background-color:#007FC8;border-bottom:solid 4px #0070B1;text-shadow:0 2px #0070B1}.button-contato:hover{transition:.2s;-webkit-transition:.2s}.social-button:hover{transition:.2s;-webkit-transition:.2s;transform:scale(1.2)}#porque,#portfolio,#sobre{background-color:#fff}#clientes,#contato,#servicos{background-color:#f3f3f3}#porque{color:#666}#contato{color:#666;text-align:center}.float-left{float:left}.conteudo-port{padding:25px}#clientes,#sobre{padding-top:50px;color:#666;text-align:center}.texto-port{font-size:16px}.conteudo-port-left{width:60%;float:left}.conteudo-port-right{width:40%;float:right}.button-port{font-size:20px}#sobre{padding-bottom:75px}#clientes{padding-bottom:50px}.sub-pequeno{font-size:10px;color:#bbb}.esquerda{width:60%;float:left}.direita{width:35%;padding:2.5%;float:right}.cliente-img{border-radius:100px}.titulo{font-size:40px;font-weight:900;color:#666;width:auto;margin-bottom:25px;padding:10px}.texto{font-size:24px}.item-carousel{background-color:#00A2FF;border-bottom:solid 4px #0095EA;color:#fff;padding:10px;max-height:250px;text-shadow:0 2px #0095EA;border-radius:20px}.item-carousel h3{margin:0}.item-carousel h4{margin-bottom:0}.item-carousel i{font-size:70px;margin-bottom:5px}.form-contato{background-color:#E8E8E8;border-radius:15px;padding:25px}.input-form{border-radius:5px;border:none;background-color:#fff;font-size:18px;width:95%;padding:2.5%;margin-bottom:10px;margin-top:10px;font-family:Lato}@media only screen and (max-width:768px){#logo,#menu{height:auto}.container{width:90%}#menu-itens{display:none}#logo{margin:auto;display:table;float:none}.social-button{font-size:20px;padding:10px}.wmg-details-content{min-height:325px!important;margin-bottom:25px!important}.esquerda{width:auto;margin-bottom:25px}.direita{width:80%;padding-top:25px;margin-right:auto;margin-left:auto;float:none}}@media only screen and (max-width:425px){.texto{font-size: 18px;}#logo,#menu{height:auto}.social-button{width:90%;display:block;padding:10px 0;margin:10px 5%}.conteudo-port-right{width:auto}.conteudo-port-left{display:none}.wmg-details-content{background-color:transparent!important}.texto-port{font-size:10px}.conteudo-port-right h3{font-size:14px}#social-buttons{margin:0 0 50px;width:100%}#logo{max-width:50%}#titulo-welcome{font-size:30px}#texto-welcome{font-size:18px}}</style>
</head>
<body>
<!--VIDEO BACKGROUND-->
<video autoplay muted loop id="bgvid">
  <source src="video/video.mp4" type="video/mp4">
</video>
<!--FIM VIDEO BACKGROUND-->

<!--MENU-->
<div id="menu">
  <div class="container">
    <div id="logo"><img src="images/logo-menu.png" width="100%" height="auto" alt="Logo lucasbonavina.com"/></div>
    <div id="menu-itens">
    <a href="#sobre" id="sobrebutton" class="menu-item">Sobre</a>
    <a href="#servicos" class="menu-item">Serviços</a>
    <a href="#clientes" class="menu-item">Clientes</a>
    <a href="#portfolio" class="menu-item">Portfólio</a>
    <a class="menu-item" href="#contato">Contato</a>
    </div>
  </div>
</div>

<!--SECTION DE BEM VINDO-->
<div class="conteudo">
  <div class="container" id="welcome">
    <div id="titulo-welcome"><span class="blue">FREELANCER </span><br>
      BACK-END & FRONT-END</div>
    <div id="texto-welcome"> Olá, bem vindo ao meu site, sou o Lucas Bonavina e trabalho para desenvolver aos meus clientes os melhores serviços. Abaixo você vai ver muitas outras informações a meu respeito, mas caso ainda fique dúvidas você pode visitar minhas redes sociais: </div>
    <div id="social-buttons"> <a href="https://www.facebook.com/Lucas-Bonavina-1587922284864535" target="_blank" class="social-button"><i class="fa fa-facebook-square fa-lg"></i> FACEBOOK</a> <a class="social-button" href="https://br.linkedin.com/in/lbonavina" target="_blank"><i class="fa fa-linkedin-square fa-lg"></i> LINKEDIN</a> <a class="social-button" target="_blank" href="https://plus.google.com/112180287555311492503"><i class="fa fa-google-plus-square fa-lg"></i> GOOGLE+</a> </div>
  </div>
</div>

<!--SECTION DE BEM VINDO-->
<div class="conteudo" id="porque">
  <div class="container" style="display: table">
    <div class="esquerda">
      <div class="titulo">PORQUE ME <span class="blue">ESCOLHER?</span></div>
      <div class="texto">Ao meu ver, um software ou website bem desenvolvido, é aquele que possuí uma interface agradável com funcionalidades que possam agilizar o trabalho do cliente, por isso, sempre levo estes conceitos comigo, transformando isso para o cliente. E pode ter certeza que nunca vou te deixar na mão, eu sempre cumpro os prazos especificados!</div>
    </div>
    <div class="direita"> <img src="images/img1.png" width="100%" height="auto" alt=""/> </div>
  </div>
</div>

<!--SERVIÇOS-->
<div class="conteudo" id="servicos">
<div class="container">
<div class="titulo">MEUS <span class="blue">SERVIÇOS</span></div>
<div id="owl-example" class="owl-carousel">
  <div class="item-carousel"> <i class="fa fa-laptop fa-4x"></i>
    <h3>SOFTWARES</h3>
    <h4>Desenvolvimento de Softwares para desktop. Ganhe tempo agilizando seu negócio.</h4>
  </div>
  <div class="item-carousel"> <i class="fa fa-code fa-4x"></i>
    <h3>SITES</h3>
    <h4>Criação de sites otimizados para sua empresa que quer ter um destaque no mercado.</h4>
  </div>
  <div class="item-carousel"> <i class="fa fa-th fa-4x"></i>
    <h3>LAYOUTS</h3>
    <h4>Desenvolvimento de layouts de sites ou softwares em psd para freelancers e agências!</h4>
  </div>
  <div class="item-carousel"> <i class="fa fa-paint-brush fa-4x"></i>
    <h3>BANNERS</h3>
    <h4>Criação de banners, logos, retrospectivas e convites para seu evento ou negócio.</h4>
  </div>
  <div class="item-carousel"> <i class="fa fa-android fa-4x"></i>
    <h3>APPS</h3>
    <h4>Precisando de um app para sua empresa ou negócio? Eu posso desenvolver para você!</h4>
  </div>
</div>
</div>
</div>
<div class="conteudo" id="sobre">
<div class="container">
<div class="titulo">SOBRE <span class="blue">MIM</span></div>
<div class="texto">Bom, meu nome é Lucas Bonavina, sou Paulista, tenho 18 anos, sou amante da técnologia e sempre em busca de aprender coisas novas na internet. No momento cursando faculdade de sistemas da informação, além de trabalhar como freelancer nesse tempo livre que possuo.</div>
</div>
</div>
<div class="conteudo" id="clientes">
<div class="container">
<div class="titulo">O QUE OS <span class="blue">CLIENTES FALAM?</span><br><div class="sub-pequeno">COMENTÁRIOS RETIRADOS DO WORKANA & LINKEDIN</div></div>
<div id="owl-clientes" class="owl-carousel">
<div class="item-carousel-cliente"> <img class="cliente-img" src="images/thiago.png" alt="user"/>
    <h2>Thiago de Freitas</h2>
    <h4>" Excelente profissional, atendeu todos os requisitos que eu precisava para colocar meu projeto no ar e com um preço justo! "</h4>
  </div>
  <div class="item-carousel-cliente"> <img class="cliente-img" src="images/guilherme.png" alt="user"/>
    <h2>Guilherme Ávila</h2>
    <h4>" O Lucas é um excelente profissional. Recomendo a todos que procuram um trabalho de qualidade! "</h4>
  </div>
  <div class="item-carousel-cliente"> <img class="cliente-img" src="images/paulo.png" alt="user"/>
    <h2>Paulo Ingrevallo</h2>
    <h4>" Lucas foi bem preciso e rápido na resolução do BUG, RECOMENDO! "</h4>
  </div>
  <div class="item-carousel-cliente"> <img class="cliente-img" src="images/maxwell.png" alt="user"/>
    <h2>Maxwell Magno</h2>
    <h4>" Excelente profissional, recomendo a todos do Workana! "</h4>
  </div>
  <div class="item-carousel-cliente"> <img class="cliente-img" src="images/user.png" alt="user"/>
    <h2>Alisson Pereira</h2>
    <h4>" O cara é simplesmente o melhor! Consegue realizar tarefas em muito pouco tempo. Voltarei a fechar negócio novamente "</h4>
  </div>
  <div class="item-carousel-cliente"> <img class="cliente-img" src="images/eduardo.png" alt="user"/>
    <h2>Eduardo Correia</h2>
    <h4>" Me pediu 2 dias e entregou em 2 horas, indico a todos "</h4>
  </div>
</div>
</div>
</div>
<div class="conteudo" id="portfolio">
<div class="container">
<div class="titulo">MEU <span class="blue">PORTFÓLIO</span><br><div class="sub-pequeno">APENAS ALGUNS DOS MELHORES TRABALHOS</div></div>
<!-- container de todos os itens -->
<div class="portfolio-grid">

    <div class="wmg-item">
        <div class="wmg-thumbnail">
            <div class="wmg-thumbnail-content">
                <img src="images/p1.png" alt="patrion"/>
            </div>
        </div>
        <div class="wmg-details">
            <span class="wmg-close"></span>
            <div class="wmg-details-content" style="border-radius: 10px; padding: 10px;">
            <div class="conteudo-port">
            <div class="conteudo-port-left"><img src="images/p1-img.png" width="100%" height="auto"/></div>
            <div class="conteudo-port-right">
            <h3>PATRION - GERÊNCIA DE PATRIMÔNIOS</h3>
                <div class="texto-port">Patrion é um sistema desenvolvido para gerenciar os patrimônios de uma empresa. Você pode organizar os patrimônios por ambiente ou por responsável. Existem três tipos de contas (Administrador, Gerente e Usuário), onde cada um tem seus respectivos papéis dentro do software.</div>
                <a href="https://github.com/lbonavina/patrion-gerencia-patrimonios" target="_blank" class="social-button button-port">VER NO GITHUB</a>
            </div>
            </div>
            </div>
        </div>
    </div>
    <div class="wmg-item">
        <div class="wmg-thumbnail">
            <div class="wmg-thumbnail-content">
                <img src="images/p2.png" alt="patrion"/>
            </div>
        </div>
        <div class="wmg-details">
            <span class="wmg-close"></span>
            <div class="wmg-details-content" style="border-radius: 10px; padding: 10px;">
            <div class="conteudo-port">
            <div class="conteudo-port-left"><img src="images/p2-img.png" width="100%" height="auto"/></div>
            <div class="conteudo-port-right">
            <h3>NINA MARCHI'S - CONTROLE DE ESTOQUE</h3>
                <div class="texto-port">Esse foi um software desenvolvido para um cliente que necessitava de um controle de entrada e saída de seus produtos. O Software faz isso, usando como se fosse simular uma venda ou compra de produtos, ainda tem funções adicionais inclusas.</div>
                <a href="http://imgur.com/a/2yJX9" target="_blank" class="social-button button-port">FOTOS</a>
            </div>
            </div>
            </div>
        </div>
    </div>
    <div class="wmg-item">
        <div class="wmg-thumbnail">
            <div class="wmg-thumbnail-content">
                <img src="images/p3.png" alt="patrion"/>
            </div>
        </div>
        <div class="wmg-details">
            <span class="wmg-close"></span>
            <div class="wmg-details-content" style="border-radius: 10px; padding: 10px;">
            <div class="conteudo-port">
            <div class="conteudo-port-left"><img src="images/p3-img.png" width="100%" height="auto"/></div>
            <div class="conteudo-port-right">
            <h3>QRSCANNER - LEITOR QR</h3>
                <div class="texto-port">QRScanner é um app desenvolvido para leitura de códigos QR. Simples e com uma interface bonita no estilo material design.</div>
                <a href="http://www.amazon.com.br/dp/B01LKJRXVS" target="_blank" class="social-button button-port">DOWNLOAD AMAZON</a>
            </div>
            </div>
            </div>
        </div>
    </div>
    <div class="wmg-item">
        <div class="wmg-thumbnail">
            <div class="wmg-thumbnail-content">
                <img src="images/p4.png" alt="patrion"/>
            </div>
        </div>
        <div class="wmg-details">
            <span class="wmg-close"></span>
            <div class="wmg-details-content" style="border-radius: 10px; padding: 10px;">
            <div class="conteudo-port">
            <div class="conteudo-port-left"><img src="images/p4-img.png" width="100%" height="auto"/></div>
            <div class="conteudo-port-right">
            <h3>QUANTUM EXPERT</h3>
                <div class="texto-port">Quantum Expert é um blog desenvolvido na plataforma Wordpress com um design limpo e elegante. Acesse o site para um visualização completa.</div>
                <a href="http://quantumexpert.comli.com/" target="_blank" class="social-button button-port">ACESSAR SITE</a>
            </div>
            </div>
            </div>
        </div>
    </div>

</div>
</div>
</div>

<div class="conteudo" id="contato">
<div class="container">
<div class="titulo">ENTRE EM <span class="blue">CONTATO</span></div>
<form action="email.php" method="post" class="form-contato">
<input type="email" name="email" placeholder="DIGITE SEU E-MAIL" class="input-form" required />
<input type="text" name="nome" placeholder="DIGITE SEU NOME COMPLETO" class="input-form" required />
<select name="assunto" placeholder="SELECIONE O ASSUNTO" class="input-form" style="width: 100%" required>
<option value="Desenvolvimento Aplicativo">SELECIONE UM ASSUNTO</option>
  <option value="Desenvolvimento Aplicativo">Desenvolvimento de Aplicativo</option>
  <option value="Desenvolvimento Software">Desenvolvimento de Software</option>
  <option value="Desenvolvimento Site">Desenvolvimento de Site</option>
  <option value="Logo ou Arte visual">Logotipos ou Arte visual</option>
  <option value="Outro">Outro</option>
</select>
<textarea name="mensagem" class="input-form" placeholder="DIGITE AQUI A SUA MENSAGEM" rows="5" required>
</textarea>
<input type="submit" class="button-contato" value="ENVIAR" />
</form>
</div>
</div>
<div class="conteudo" id="footer">
  COPYRIGHT © 2016 - LUCASBONAVINA.COM
</div>
</body>
</html>
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link href="owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="owl-carousel/owl.theme.css" rel="stylesheet">
<link rel="stylesheet" href="css/jquery.wm-gridfolio-1.0.min.css">
<script src="owl-carousel/assets/js/jquery.min.js"></script>
<script src="owl-carousel/owl.carousel.js"></script>
<script>
    $(document).ready(function($) {
      $("#owl-example").owlCarousel();
	  $("#owl-clientes").owlCarousel({
	  items: 3
	  });
	  $('.portfolio-grid').WMGridfolio();
    });
    $(document).ready(function() {
    $('[href="#contato"]').click(function() {
        $("html,body").animate({
            scrollTop: $("#contato").offset().top
        }, 1200, "swing");
        return false;
    });
    $('[href="#sobre"]').click(function() {
        $("html,body").animate({
            scrollTop: $("#sobre").offset().top
        }, 1200, "swing");
        return false;
    });
    $('[href="#servicos"]').click(function() {
        $("html,body").animate({
            scrollTop: $("#servicos").offset().top
        }, 1200, "swing");
        return false;
    });
    $('[href="#portfolio"]').click(function() {
        $("html,body").animate({
            scrollTop: $("#portfolio").offset().top
        }, 1200, "swing");
        return false;
    });
    $('[href="#clientes"]').click(function() {
        $("html,body").animate({
            scrollTop: $("#clientes").offset().top
        }, 1200, "swing");
        return false;
    });
});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84316120-1', 'auto');
  ga('send', 'pageview');

</script>
<script src="js/jquery.wm-gridfolio-1.0.min.js"></script>

<!-- Hosting24 Analytics Code -->
<script type="text/javascript" src="http://stats.hosting24.com/count.php"></script>
<!-- End Of Analytics Code -->
